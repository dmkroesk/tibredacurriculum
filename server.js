'use strict';

process.env.NODE_ENV !== 'production' ?
    process.env.NODE_ENV = 'development':
    null;

const express = require('express');
const ApiError = require('./ApiError');
const bodyParser = require('body-parser');
const AuthController = require('./controllers/auth.controller');
const TiBredaRoutes = require('./routes/tibreda.routes');
const TiBredaAuthRoutes = require('./routes/auth.routes');
const _ = require('./database/db.connector');
const cors = require('cors');

const port = process.env.TIBREDA_BACKEND_PORT || 3000;
const httpSchemes = process.env.NODE_ENV === 'production' ? ['https'] : ['http']
const app = express();
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

const expressSwagger = require('express-swagger-generator')(app);

const options = {
    swaggerDefinition : {
        info: {
            title: 'TI Breda curriculum API',
            version: '1.0.0',
            description: '<p>TI Breda curriculum API</p>'
        },
        host: port,
        basePath: '',
        produces: [
            'application/json'
        ],
        securityDefinition: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'x-access-token',
                description: 'register request dkroeske@gmail.com'
            }
        },
        schemes: httpSchemes
    },
    basedir: __dirname,
    files:['./routes/**/*.js']
};
expressSwagger(options);

//
app.use(cors());

// Unprotected routes
app.use('/v1', TiBredaAuthRoutes);

// Protected Routes, always check token
app.use('/v1', AuthController.validateToken);

//
app.use('/v1', TiBredaRoutes );

//
app.use('*', (req, res, next) => {
    next( new ApiError('Non-existing endpoint', 404) );
});

app.use( '*', (err, req, res, next) => {
    res.status(err.code || 404).json(err).end();
});

// Netjes afsluiten
function shutdown() {

    if( process.env.NODE_ENV === 'production') {
        // Handle production stuff
    }
}

process.on('SIGTERM', shutdown);
process.on('SIGINT', shutdown);

const server = app.listen(port, () => {
    console.log('The magic happens at port ' + server.address().port )
});

module.exports = server;