# TI Breda Curriculum API
Nodejs server die een api biedt het TI curriculum

## Documentation
See the Swagger documentation at [our cloudserver](http://xxxx:3000/api-docs/) and [localhost](http://localhost:3000/api-docs/).

## Contents
This server contains:

## Requirements
- nodejs
- mongodb

## Usage
- Fork this repo and clone your copy onto your local machine.

Then run

```
npm install
npm start
```

For testing:
```
npm test
```
