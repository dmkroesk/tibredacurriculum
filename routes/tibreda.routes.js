'use strict';

let routes = require('express').Router();
const TiBredaController = require('../controllers/tibreda.controller');

/**
 * @typedef ApiError
 * @property {string} message.required
 * @property {integer} code.required
 * @property {string} datetime.required
 */

/**
 * @typedef ValidToken
 * @property {string} token.required
 * @property {string} email.required
 */

/**
 * Lees single onderwijs module uit het TI curriculum.
 *
 * @route GET /api/module
 * @group Modules - Endpoints voor informatie over de TI onderwijs modules.
 * @returns {ValidToken.model} 200.OK - Token informatie
 * @returns {ApiError.model} 401 - Niet geautoriseerd (geen valid token)
 * @returns {ApiError.model} 412 - Een of meer properties in de request body ontbreken of zijn foutief
 */
routes.get('/module', TiBredaController.readModule );

/**
 * Lees multiple onderwijs modulen uit het TI curriculum.
 *
 * @route GET /api/modules
 * @group Modules - Endpoints voor informatie over de TI onderwijs modules.
 * @returns {ValidToken.model} 200.OK - Token informatie
 * @returns {ApiError.model} 401 - Niet geautoriseerd (geen valid token)
 * @returns {ApiError.model} 412 - Een of meer properties in de request body ontbreken of zijn foutief
 */
routes.get('/modules', TiBredaController.readModules );

/**
 * Update single onderwijs module uit het TI curriculum.
 *
 * @route PUT /api/module
 * @group Modules - Endpoints voor informatie over de TI onderwijs modules.
 * @returns {ValidToken.model} 200.OK - Token informatie
 * @returns {ApiError.model} 401 - Niet geautoriseerd (geen valid token)
 * @returns {ApiError.model} 412 - Een of meer properties in de request body ontbreken of zijn foutief
 */
routes.put('/module', TiBredaController.updateModule );

/**
 * Maak single onderwijs module uit het TI curriculum aan.
 *
 * @route POST /api/module
 * @group Modules - Endpoints voor informatie over de TI onderwijs modules.
 * @returns {ValidToken.model} 200.OK - Token informatie
 * @returns {ApiError.model} 401 - Niet geautoriseerd (geen valid token)
 * @returns {ApiError.model} 412 - Een of meer properties in de request body ontbreken of zijn foutief
 */
routes.post('/module', TiBredaController.createModule );

/**
 * Verwijder single onderwijs module uit het TI curriculum.
 *
 * @route DELETE /api/module
 * @group Modules - Endpoints voor informatie over de TI onderwijs modules.
 * @returns {ValidToken.model} 200.OK - Token informatie
 * @returns {ApiError.model} 401 - Niet geautoriseerd (geen valid token)
 * @returns {ApiError.model} 412 - Een of meer properties in de request body ontbreken of zijn foutief
 */
routes.delete('/module', TiBredaController.deleteModule );


module.exports = routes;