const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

//
// Check Production vs Development mode
//
let dbUrl = '';
if( process.env.NODE_ENV === 'production') {
    const user = process.env.EMON_DB_USER;
    const pwd = process.env.EMON_DB_PASSWORD;
    dbUrl = 'mongodb://' + user + ':' + pwd + '@localhost:27017/tibreda';
} else {
    dbUrl = 'mongodb://localhost:27017/tibreda';
}

//
// Export connection
//
dbConnector = function(mongoUri) {

    return mongoose.connect( mongoUri, {
        useNewUrlParser: true,
    })
        .then( db => {
            console.log('Connected to db ' );
        })
        .catch( error => {
            console.warn('Warning', error.toString());
            throw error;
        });

} (dbUrl);

module.exports = dbConnector;
