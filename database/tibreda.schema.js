const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tibredaSchema = new Schema({

        opleiding: {
            type: String,
            required: true,
            maxlength: [64, 'Max 64'],
        },

        imageurl: {
            type: Array,
            required: false,
            validate: {
                validator : function(array) {
                    return array.every( (v) => typeof v === 'string' )
                }
            }
        },

        naam: {
            type: String,
            required: true,
            maxlength: [64, 'Max 64'],
        },

        omschrijving: {
            type: String,
            required: true,
            maxlength: [255, 'Max 255'],
        },

        osirisCode: {
            type: String,
            required: true,
            unique: true,
            minlength: [4, 'Min 4'],
            maxlength: [64, 'Max 64'],
        },

        leerjaar: {
            type: Number,
            required: true,
            min: 1,
            max: 4,
        },

        periode: {
            type: Number,
            required: true,
            min: 1,
            max: 4
        },

        leerlijn: {
            type: String,
            required: false,
            maxlength: [64, 'Max 64']
        },

        eindkwalificaties: {
            gebruikersinteractie: {
                CHZ: { type: String, required: false, maxlength: [1, '1'] },
                VZ: { type: String, required: false, maxlength: [1, '1'] },
                GI: { type: String, required: false, maxlength: [1, '1'] }
            },
            bedrijfsprocessen: {
                CHZ: { type: String, required: false, maxlength: [1, '1'] },
                VZ: { type: String, required: false, maxlength: [1, '1'] },
                GI: { type: String, required: false, maxlength: [1, '1'] }
            },
            infrastructuur: {
                CHZ: { type: String, required: false, maxlength: [1, '1'] },
                VZ: { type: String, required: false, maxlength: [1, '1'] },
                GI: { type: String, required: false, maxlength: [1, '1'] }
            },
            software: {
                CHZ: { type: String, required: false, maxlength: [1, '1'] },
                VZ: { type: String, required: false, maxlength: [1, '1'] },
                GI: { type: String, required: false, maxlength: [1, '1'] }
            },
            hardwareInterfacing: {
                CHZ: { type: String, required: false, maxlength: [1, '1'] },
                VZ: { type: String, required: false, maxlength: [1, '1'] },
                GI: { type: String, required: false, maxlength: [1, '1'] }
            },
        },

        studiepunten: {
            type: Number,
            required: true,
            min: 1,
            max: 15,
        },

        docent: {
            type: Array,
            required: true,
            validate: {
                validator : function(array) {
                    return array.every( (v) => typeof v === 'string' )
                }
            }
        },

        delivables: {
            type: String,
            required: true,
            minlength: 1,
            maxlength: [64, 'Max 64'],
        },

        leerdoelen: {
            type: Array,
            required: true,
            validate: {
                validator : function(array) {
                    return array.every( (v) => typeof v === 'string' )
                }
            }
        },

        onderwijsvorm: {
            type: String,
            required: false,
            maxlength: [64, 'Max 64'],
        },

        leermiddelen: {
            type: String,
            required: true,
            maxlength: [64, 'Max 64'],
        },

        toetsing: {
            type: String,
            required: true,
            maxlength: [64, 'Max 64'],
        },

        herkansing: {
            type: String,
            required: true,
            maxlength: [64, 'Max 64'],
        },

        bijzonderheden: {
            type: String,
            required: false,
            maxlength: [255, 'Max 255'],
        }
    },
    {
        timestamps: true,

        toJSON: {
            transform: (doc, ret) => {
                delete ret._id;
                delete ret.__v;
            }
        }
    }
);

const TiBredaModuleSchema = mongoose.model('tibreda', tibredaSchema);

module.exports = TiBredaModuleSchema;