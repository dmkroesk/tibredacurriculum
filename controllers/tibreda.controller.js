'use strict';

const ApiError =  require('../ApiError');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const TiBredaModuleSchema = require('../database/tibreda.schema');
const OnderwijsModule = require('../model/module.model');

module.exports = {

    // CRUD: create one
    /**
     * Create a new TI module.
     *
     * @param {*} req The incoming request.
     * @param {*} res The newly created person.
     * @param {*} next ApiError when id is invalid.
     */
    createModule( req, res, next ) {
        //
        // let m = new OnderwijsModule(req.body);
        // console.log(m);
        let module = new TiBredaModuleSchema(req.body);
        module.save()
            .then( () => res.status(200).json().end() )
            .catch( (error) => next(new ApiError(error.toString(), 500)) );
    },

    /**
     * Read single TI module.
     *
     * @param {*} req The incoming request.
     * @param {*} res The newly created person.
     * @param {*} next ApiError when id is invalid.
     */
    readModule( req, res, next ) {
        //
        let query = {};

        // Query
        TiBredaModuleSchema
            .find(query, {})
            .sort('-createdAt')
            .limit(100)
            .then((rows) => {
                res.status(200).json(rows);
            })
            .catch((error) => {
                next(new ApiError(error.toString(), 500))
            });
    },

    /**
     * Read multiple TI modules.
     *
     * @param {*} req The incoming request.
     * @param {*} res The newly created person.
     * @param {*} next ApiError when id is invalid.
     */
    readModules( req, res, next ) {
        next();
    },

    /**
     * Update single TI module.
     *
     * @param {*} req The incoming request.
     * @param {*} res The newly created person.
     * @param {*} next ApiError when id is invalid.
     */
    updateModule( req, res, next ) {
        next();
    },

    /**
     * Delete single TI module.
     *
     * @param {*} req The incoming request.
     * @param {*} res The newly created person.
     * @param {*} next ApiError when id is invalid.
     */
    deleteModule( req, res, next ) {
        next();
    },
};